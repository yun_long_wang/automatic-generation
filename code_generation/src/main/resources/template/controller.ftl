package ${package}.${moduleName}.controller;

import com.qdone.framework.core.BaseController;
import ${package}.${moduleName}.model.${className};
import ${package}.${moduleName}.service.${className}Service;
import com.qdone.support.async.log.db.annotation.ActionLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* ${classname}管理
* ${comments}
* @${author}
* @email ${email}
* @date ${datetime}
*/
@Controller
@RequestMapping("/${classname}")
@Api(tags = "${comments}管理",description = "${comments}信息管理")
public class ${className}Controller extends BaseController{

@Autowired
private ${className}Service ${classname}Service;

/**
* 页面初始化
* @return 页面初始化
*/
@ApiOperation(value = "${comments}列表",notes = "进入${comments}列表页", httpMethod = "GET")
@RequestMapping(value = "init",method = RequestMethod.GET)
public String init(){
    return "${classname}/select${className}";
}

/**
* 分页查询
* @param entity 查询参数
* @return 分页查询结果
*/
@RequestMapping(value="/selectPage",method=RequestMethod.POST)
@ResponseBody
@ApiOperation(value = "${comments}分页列表", notes = "${comments}分页列表", httpMethod = "POST",response = Map.class)
@ActionLog(moudle = "${comments}管理",actionType = "${comments}分页列表")
public Map<String, Object> selectPage(@RequestBody ${className} entity){
    return responseSelectPage(${classname}Service.selectPage(entity));
}

/**
* 跳转添加
* @param request 请求参数
* @return 跳转添加页面
*/
@RequestMapping(value="/preInsert",method=RequestMethod.GET)
@ApiOperation(value = "跳转${comments}添加", notes = "进入${comments}添加页面", httpMethod = "GET")
public String preInsert(HttpServletRequest request){
    return "${classname}/insert${className}";
}

/**
* 添加数据
* @param entity 对象参数
* @return 添加数据
*/
@RequestMapping(value="/insert",method=RequestMethod.PUT)
@ResponseBody
@ActionLog(moudle = "${comments}管理",actionType = "创建${comments}")
@ApiOperation(value = "${comments}添加", notes = "创建${comments}", httpMethod = "PUT",response = Boolean.class)
public Boolean insert(@ApiParam(name = "${comments}对象", value = "传入json格式", required = true)  @RequestBody ${className} entity) {
    return ${classname}Service.insert(entity).getOperateResult()>0?Boolean.TRUE:Boolean.FALSE;
}

/**
* 跳转更新
*/
@RequestMapping(value="/preUpdate",method=RequestMethod.GET)
@ApiOperation(value = "跳转${comments}更新", notes = "进入${comments}更新页面", httpMethod = "GET")
public ModelAndView preUpdate( @RequestParam(value="${pk.attrname}", required=true) ${pk.attrType} ${pk.attrname}){
Map<String,Object> param=new HashMap<>();
param.put("${classname}",${classname}Service.view(${pk.attrname}));
    return new ModelAndView("${classname}/update${className}",param);
}

/**
* 更新数据
* @param entity 对象参数
* @return 更新数据
*/
@RequestMapping(value="/update",method=RequestMethod.POST)
@ResponseBody
@ActionLog(moudle = "${comments}管理",actionType = "更新${comments}")
@ApiOperation(value = "更新${comments}", notes = "更新${comments}信息", httpMethod = "POST",response = Boolean.class)
public Boolean update(@ApiParam(name = "${comments}对象", value = "传入json格式", required = true) ${className} entity) {
    return ${classname}Service.update(entity).getOperateResult()>0?Boolean.TRUE:Boolean.FALSE;
}

/**
* 删除数据
* @param ids 主键集合
* @return 删除数据
*/
@RequestMapping(value="/delete",method=RequestMethod.POST)
@ResponseBody
@ActionLog(moudle = "${comments}管理",actionType = "删除${comments}")
@ApiOperation(value = "删除${comments}",notes = "删除${comments}", httpMethod = "POST",response = Boolean.class)
public Boolean delete(@RequestBody List<${className}> ids) {
    return ${classname}Service.batchDelete(ids)>0?Boolean.TRUE:Boolean.FALSE;
}

}
