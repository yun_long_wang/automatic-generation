<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>修改${classname}</title>
    <meta name="description" content="" />
    <meta name="author" content="pc" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1, minimum-scale=1, user-scalable=no" />
    <meta name="screen-orientation"content="portrait">
    <meta name="format-detection" content="telephone=no" />
    <#noparse><#include "/inc/common.html"></#noparse>
</head>
<body>
<div style="margin-bottom: 10px">
    <input type="button" value="返回" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;" class="btn-primary" onclick="${classname}Back()" />
    <input type="button" value="保存" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;" class="btn-primary" onclick="${classname}Save()" />
    <input type="button" value="取消" style="width: 80px; height: 30px; vertical-align: bottom; line-height: 10px;" class="btn-primary" onclick="${classname}Cancle()" />
</div>
<div>
    <form id="${classname}UpdateFm" name="${classname}UpdateFm"  method="post" action="">
        <input type="hidden" name="${pk.attrname}" value="<#noparse>${</#noparse>${classname}.${pk.attrname}<#noparse>}</#noparse>"/>
        <table border="0" cellpadding="0"  cellspacing="0"
               class="formlist" style="width: 100%; table-layout: fixed;">
${updateFormParam}
        </table>
    </form>
</div>
<script type="text/javascript">
    function ${classname}Save(){//保存
        if($('#${classname}UpdateFm').valid()){//验证通过
            $.ajax({
                url: '<#noparse>${request.contextPath}</#noparse>/${classname}/update',
                contentType : "application/x-www-form-urlencoded; charset=UTF-8",
                datatype: 'json',
                type:"post",
                data:$('#${classname}UpdateFm').serializeObject(),
                success: function(json, status) {
                    if(json==true){
                        layer.msg('修改成功', {
                            icon:1,
                            time: 500,
                            end: function(index, layero){
                                parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
                                window.parent.location.reload(); //刷新父页面
                            }
                        });
                    }else{
                        layer.msg('修改失败', {
                            icon:5,
                            time: 500,
                            end: function(index, layero){
                                parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
                                window.parent.location.reload(); //刷新父页面
                            }
                        });
                    }
                },
                error: function(json, status) {
                    layer.msg('系统异常,请稍后重试或联系技术人员', {
                        icon:5,
                        time: 1500,
                        end: function(index, layero){
                            parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
                            window.parent.location.reload(); //刷新父页面
                        }
                    });
                }
            });
        }
    }
    function ${classname}Cancle(){//取消
        $(':input','#${classname}UpdateFm')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    }
    function ${classname}Back(){//返回
        parent.layer.close(parent.layer.getFrameIndex(window.name));  // 关闭layer
        window.parent.location.reload(); //刷新父页面
    }
</script>
</body>
</html>
