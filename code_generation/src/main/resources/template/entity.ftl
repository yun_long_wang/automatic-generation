package ${package}.${moduleName}.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.qdone.framework.core.page.MutiSort;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import lombok.*;
<#if hasDate?? && hasDate>
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
</#if>
<#if hasBigDecimal?? && hasBigDecimal>
import java.math.BigDecimal;
</#if>

/**
* ${comments}
*
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "${comments}", description = "${className}实体类")
public class ${className} extends MutiSort {

	private static final long serialVersionUID = 1L;

<#if columns?? &&columns?size gt 0>
	<#list columns as column>
	/**
	* ${column.comments}
	*/
		<#if column.nullAble?? &&column.nullAble=='true'&& column.columnName?? && pk.columnName?? && column.columnName != pk.columnName>
	@NotBlank(message="${column.comments}不能为空")
		</#if>
		<#if column.nullAble?? && column.nullAble=='true'>
	@ApiModelProperty(value = "${column.comments}",required = true,position=${column_index})
		<#else>
	@ApiModelProperty(value = "${column.comments}",required = false,position=${column_index})
		</#if>
		<#if column.attrType?? && column.attrType=='Date'>
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
		</#if>
	<#if column.defaultValue?? && column.defaultValue!=null >
	<#if column.attrType?? && column.attrType=='String'>
	private ${column.attrType} ${column.attrname} ="${column.defaultValue}";
	<#elseif column.attrType?? && column.attrType=='Date' && column.defaultValue=='CURRENT_TIMESTAMP'>
	private ${column.attrType} ${column.attrname} =new Date();
	<#else>
	private ${column.attrType} ${column.attrname} =${column.defaultValue};
	</#if>
	<#else>
	private ${column.attrType} ${column.attrname};
	</#if>
	</#list>
</#if>
}
