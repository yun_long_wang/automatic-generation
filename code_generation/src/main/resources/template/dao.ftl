package  ${package}.${moduleName}.dao;
import ${package}.${moduleName}.model.${className};
import java.util.List;

/**
* ${classname}服务接口
* ${comments}
* @author ${author}
* @email ${email}
* @date ${datetime}
*/
public interface ${className}Dao {

/**
* 查询列表
* @param entity 查询参数
* @return 查询列表结果
*/
public List<${className}> select(${className} entity);


/**
* 根据主键查看
* @param pk 主键
* @return 根据主键查询结果
*/
public ${className} view(${pk.attrType} pk);

/**
* 新增(非空字段)
* @param entity 新增参数
* @return 新增操作结果
*/
public int insert(${className} entity);

/**
* 新增(全部字段)
* @param entity 新增参数
* @return 新增操作结果
*/
public int insertAllColumns(${className} entity);

/**
* 修改(非空字段)
* @param entity 更新参数
* @return 更新操作结果
*/
public int update(${className} entity);

/**
* 更新(全部字段)
* @param entity 更新参数
* @return 更新操作结果
*/
public int updateAllColumns(${className} entity);

/**
* 新增或更新(主键必填)
* @param entity 待处理参数
* @return 处理结果
*/
public int merge(${className} entity) ;


/**
* 批量添加或修改
* @param arr 数据
* @return 批量添加或修改
*/
public int batchMerge(List<${className}> arr);

/**
* 删除数据
* @param pk 主键
* @return 执行结果
*/
public int delete(${pk.attrType} pk);

/**
* 删除
* @param pkList 主键集合
* @return 根据主键集合删除数据
*/
public int batchDelete(List<${className}> pkList);

}
