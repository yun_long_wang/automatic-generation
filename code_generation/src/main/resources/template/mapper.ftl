<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.${moduleName}.dao.${className}Dao">

    <resultMap id="${className}Map" type="${package}.${moduleName}.model.${className}">
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
        <result column="${column.columnName}" jdbcType="${column.dataType}" property="${column.attrname}" />
            </#list>
        </#if>
    </resultMap>

    <sql id="allColumns">
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
        <#if column_index+1<columns?size>${column.columnName},<#else>${column.columnName}</#if>
            </#list>
        </#if>
    </sql>

    <sql id="dynamicWhere">
        <trim  suffixOverrides="," prefix="WHERE" prefixOverrides="AND">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String' && pk.columnName?? && pk.columnName == column.columnName>
            <if test="${column.attrname}!=null and ${column.attrname}!=''">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String' && pk.columnName?? && pk.columnName!=column.columnName>
            <if test="${column.attrname}!=null and ${column.attrname}!=''">AND ${column.columnName} LIKE concat('%', <#noparse>#{</#noparse>${column.attrname}<#noparse>} ,'%')</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String' && pk.columnName?? && pk.columnName == column.columnName>
            <if test="${column.attrname}!=null ">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String' && pk.columnName?? && pk.columnName != column.columnName>
            <if test="${column.attrname}!=null ">AND ${column.columnName} = <#noparse>#{</#noparse>${column.attrname}<#noparse>}</if></#noparse>
                    </#if>
                </#list>
            </#if>
        </trim>
    </sql>

    <!-- select -->
    <select id="select" parameterType="${package}.${moduleName}.model.${className}"  resultMap="${className}Map">
        SELECT <include refid="allColumns" />
        FROM ${tableName}  <include refid="dynamicWhere" />
    </select>

    <!-- view -->
    <select id="view" parameterType="${pk.propType}"  resultMap="${className}Map">
        SELECT <include refid="allColumns" />
        FROM ${tableName} WHERE ${pk.columnName} =<#noparse>#{</#noparse>${pk.attrname},jdbcType=${pk.dataType}<#noparse>}</#noparse>
    </select>

    <!-- insert -->
    <insert id="insert" <#if pk.extra?? && pk.extra =='auto_increment'>useGeneratedKeys="true" keyProperty="${pk.columnName}"</#if> parameterType="${package}.${moduleName}.model.${className}" >
        INSERT INTO ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
            <if test="${column.attrname}!=null and ${column.attrname}!=''">${column.columnName},</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
            <if test="${column.attrname}!=null">${column.columnName},</if>
                   </#if>
                </#list>
            </#if>
        </trim>
        VALUES
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
            <if test="${column.attrname}!=null and ${column.attrname}!=''"><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
            <if test="${column.attrname}!=null"><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    </#if>
                </#list>
            </#if>
        </trim>
    </insert>

    <!-- insertAllColumns -->
    <insert id="insertAllColumns" <#if pk.extra?? && pk.extra =='auto_increment'>useGeneratedKeys="true" keyProperty="${pk.columnName}"</#if> parameterType="${package}.${moduleName}.model.${className}" >
        INSERT INTO ${tableName} (<include refid="allColumns" />)
            VALUES(
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
                <#if column_index+1<columns?size><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,<#else><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse></#if>
            </#list>
        </#if>
        )
    </insert>

    <!-- update -->
    <update id="update" parameterType="${package}.${moduleName}.model.${className}" >
        UPDATE  ${tableName}
        <trim prefix="set" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
            <if test="${column.attrname}!=null and ${column.attrname}!=''">${column.columnName}=<#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
            <if test="${column.attrname}!=null">${column.columnName}=<#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    </#if>
                </#list>
            </#if>
        </trim>
        WHERE ${pk.columnName}=<#noparse>#{</#noparse>${pk.attrname},jdbcType=${pk.dataType}<#noparse>}</#noparse>
    </update>

    <!-- updateAllColumns -->
    <update id="updateAllColumns" parameterType="${package}.${moduleName}.model.${className}" >
        UPDATE  ${tableName}
        SET
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column_index+1<columns?size && column.columnName?? && pk.columnName?? && column.columnName!=pk.columnName >
            ${column.columnName}=<#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,
                    <#elseif column_index+1==columns?size && column.columnName?? && pk.columnName?? && column.columnName!=pk.columnName >
            ${column.columnName}=<#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>
                    </#if>
                </#list>
            </#if>
        WHERE ${pk.columnName}=<#noparse>#{</#noparse>${pk.attrname},jdbcType=${pk.dataType}<#noparse>}</#noparse>
    </update>

    <!-- merge -->
    <insert id="merge" parameterType="${package}.${moduleName}.model.${className}" >
        INSERT INTO ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
               <if test="${column.attrname}!=null and ${column.attrname}!=''">${column.columnName},</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
               <if test="${column.attrname}!=null">${column.columnName},</if>
                    </#if>
                </#list>
            </#if>
        </trim>
        VALUES
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
               <if test="${column.attrname}!=null and ${column.attrname}!=''"><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
               <if test="${column.attrname}!=null"><#noparse>#{</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,</if>
                    </#if>
                </#list>
            </#if>
        </trim>
        ON DUPLICATE KEY UPDATE
        <trim suffixOverrides=",">
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
                    <#if column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType=='String'>
                <if test="${column.attrname}!=null and ${column.attrname}!=''">${column.columnName}=VALUES(${column.columnName}),</if>
                    <#elseif column.attrname?? && column.attrname!=null && column.attrname!="" && column.attrType?? && column.attrType!='String'>
                <if test="${column.attrname}!=null">${column.columnName}=VALUES(${column.columnName}),</if>
                    </#if>
                </#list>
            </#if>
        </trim>
    </insert>

    <!-- batchMerge -->
    <insert id="batchMerge"  parameterType="java.util.List">
        INSERT INTO ${tableName} (
            <#if columns?? &&columns?size gt 0>
                <#list columns as column>
            <#if column_index+1<columns?size>${column.columnName},<#else>${column.columnName}</#if>
                </#list>
            </#if>
        )
        VALUES
        <foreach collection="list" item="item" index="index" separator=",">
            (
                <#if columns?? &&columns?size gt 0>
                    <#list columns as column>
              <#if column_index+1<columns?size><#noparse>#{item.</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse>,<#else><#noparse>#{item.</#noparse>${column.attrname},jdbcType=${column.dataType}<#noparse>}</#noparse></#if>
                    </#list>
                </#if>
            )
        </foreach>
           ON DUPLICATE KEY UPDATE
        <#if columns?? &&columns?size gt 0>
            <#list columns as column>
           <#if column_index+1<columns?size>${column.columnName}=VALUES(${column.columnName}),<#else>${column.columnName}=VALUES(${column.columnName})</#if>
            </#list>
        </#if>
    </insert>

    <!-- delete -->
    <delete id="delete" parameterType="${pk.propType}">
        DELETE FROM  ${tableName}  WHERE ${pk.columnName} =<#noparse>#{</#noparse>${pk.attrname},jdbcType=${pk.dataType}<#noparse>}</#noparse>
    </delete>

    <!-- batchDelete -->
    <delete id="batchDelete" parameterType="java.util.List">
        DELETE FROM  ${tableName}  WHERE ${pk.columnName}  IN
        <foreach collection="list" index="index" item="item" open="(" separator="," close=")">
            <#noparse>#{item.</#noparse>${pk.columnName},jdbcType=${pk.dataType}<#noparse>}</#noparse>
        </foreach>
    </delete>

</mapper>