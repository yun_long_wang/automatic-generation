# Automatic generation

#### 介绍
使用springBoot+freemarker开发的在线代码生成工具

#### 获取帮助

1. 工具源码：https://gitee.com/yun_long_wang/automatic-generation
2. 代码生成工具，采用springBoot+freemarker+vue+adminLTE，可以一键生成html,controller,service,mapper,entity代码
3. 您需要在mysql数据库中设计好表结构(需要主键),然后使用工具生成代码，只需要下载粘贴到配套项目对应目录，即可完成开发任务！
4. 如需关注项目最新动态，请Watch、Star项目，有问题也可以提出Issues,都是对项目最好的支持
#### 注意事项

1. 仅支持mysql数据库
2. 设计好数据库表结构（表备注，字段备注尽量全面，页面会需要，主键必须设置）。
3. 生成的zip文件解压之后，注意文件是否为BOM的UTF-8格式，可以使用nodepad++转成无BOM的UTF-8粘贴到对应项目。
4. 工具生成的压缩文件，解压之后拷贝到本地项目，如果是IDEA请注意配置JAVA->java complier为Eclipse模式,防止文件为BOM的UTF-8格式，导致IDEA编译出错！
